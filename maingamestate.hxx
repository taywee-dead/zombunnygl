/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef MAINGAME_STATE_HXX
#define MAINGAME_STATE_HXX
#include <vector>
#include <sstream>

#include <Box2D/Box2D.h>

#include "gamecore.hxx"
#include "state.hxx"
#include "shape.hxx"
#include "bunny.hxx"
#include "contactlistener.hxx"
#include "event.cxx"
#include "pausemenustate.hxx"
#include "fontbase.hxx"

class MainGameState : public State, public FontBase
{
    private:
        const GLfloat black[4];
        const GLfloat white[4];

        b2World* world;
        ContactListener* contactListener;
        int32 velocityIterations;
        int32 positionIterations;

        b2Vec2 gravity;
        bool doSleep;
        float scaleFactor;
        float speedFactor;
        float defaultSpeed;
        float cameraZoom;
        float cameraCenterX;
        float cameraCenterY;

        // This variable is used for the for loops, cached for performance
        unsigned int i;

        std::vector<Bunny*>* bunnyList;
        std::vector<Shape*>* shapeList;

        bool showVision;

    public:
        MainGameState(GameCore* game);

        void Create();
        void Destroy();

        void Pause();
        void Resume();

        void HandleEvents(GLFWEvent* event);
        void Process(float frameTime);
        void Render();
        void SelectCurrent()
        {
        }
};

#endif

