/*
 *  Lead Coder:     Taylor C. Richberger
 */

#include <cstdlib>
#include <ctime>
#include <string>
#include "gamecore.hxx"

int main(int argc, char** argv)
{
    std::string bunnyModel, zombieModel;
    srand(time(NULL));
    if (argc > 1)
    {
        bunnyModel = std::string(argv[1]);
        if (argc > 2)
        {
            zombieModel = std::string(argv[2]);
        }
    }
    GameCore game("ZombunnyGL (Built on " + std::string(__DATE__) + ")", bunnyModel, zombieModel);
    return (game.Run());
}
