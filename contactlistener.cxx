/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef CONTACTLISTENER_CXX
#define CONTACTLISTENER_CXX

#include "contactlistener.hxx"
#include "bunny.hxx"

void ContactListener::BeginContact(b2Contact* contact)
{
    // Register contact between bunnies
    if ((contact->GetFixtureA()->GetUserData() == (void*)1) &&
        (contact->GetFixtureB()->GetUserData() == (void*)1))
    {
        ((Bunny*)contact->GetFixtureA()->GetBody()->GetUserData())->BeginContact(((Bunny*)contact->GetFixtureB()->GetBody()->GetUserData()));
        ((Bunny*)contact->GetFixtureB()->GetBody()->GetUserData())->BeginContact(((Bunny*)contact->GetFixtureA()->GetBody()->GetUserData()));
    }

    // If bunny B enters bunny A's vision, bunny A becomes aware of bunny B
    if ((contact->GetFixtureA()->GetUserData() == (void*)2) &&
        (contact->GetFixtureB()->GetUserData() == (void*)1))
    {
        ((Bunny*)contact->GetFixtureA()->GetBody()->GetUserData())->BecomeAware(((Bunny*)contact->GetFixtureB()->GetBody()->GetUserData()));
    }

    // If bunny A enters bunny B's vision, bunny B becomes aware of bunny A
    if ((contact->GetFixtureA()->GetUserData() == (void*)1) &&
        (contact->GetFixtureB()->GetUserData() == (void*)2))
    {
        ((Bunny*)contact->GetFixtureB()->GetBody()->GetUserData())->BecomeAware(((Bunny*)contact->GetFixtureA()->GetBody()->GetUserData()));
    }
}


void ContactListener::EndContact(b2Contact* contact)
{
    // End contact between bunnies
    if ((contact->GetFixtureA()->GetUserData() == (void*)1) &&
        (contact->GetFixtureB()->GetUserData() == (void*)1))
    {
        ((Bunny*)contact->GetFixtureA()->GetBody()->GetUserData())->EndContact(((Bunny*)contact->GetFixtureB()->GetBody()->GetUserData()));
        ((Bunny*)contact->GetFixtureB()->GetBody()->GetUserData())->EndContact(((Bunny*)contact->GetFixtureA()->GetBody()->GetUserData()));
    }

    // If bunny B exits bunny A's vision, bunny A stops being aware of bunny B
    if ((contact->GetFixtureA()->GetUserData() == (void*)2) &&
        (contact->GetFixtureB()->GetUserData() == (void*)1))
    {
        ((Bunny*)contact->GetFixtureA()->GetBody()->GetUserData())->BecomeUnaware(((Bunny*)contact->GetFixtureB()->GetBody()->GetUserData()));
    }

    // If bunny A exits bunny B's vision, bunny B stops being aware of bunny A
    if ((contact->GetFixtureA()->GetUserData() == (void*)1) &&
        (contact->GetFixtureB()->GetUserData() == (void*)2))
    {
        ((Bunny*)contact->GetFixtureB()->GetBody()->GetUserData())->BecomeUnaware(((Bunny*)contact->GetFixtureA()->GetBody()->GetUserData()));
    }
}

#endif
