/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef BUNNY_HXX
#define BUNNY_HXX

#include <cmath>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include <iostream>

#include <Box2D/Box2D.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glext.h>

class Bunny
{
    private:
        const static float PI = 3.1415926535;

        b2Body* body;
        b2Fixture* bunnyFixture;
        b2Fixture* visionFixture;
        enum
        {
            SPHERE = 0,
            BUNNYHEAD,
            BUNNYEYES,
            ZOMBIEHEAD,
            ZOMBIEEYES,
            VAOCOUNT
        };
        enum
        {
            Vertices = 0,
            Normals,
            Indices,
            NumVBOs
        };
        static GLuint buffers[VAOCOUNT][NumVBOs];
        static GLuint indexCount[VAOCOUNT];
        static GLuint VAO[VAOCOUNT];

        bool female;
        bool zombie;
        bool seesZombie;
        bool seesBunny;
        float angle;

        // These two variables are used for the for loops, cached for performance
        unsigned int i;
        float j;

        std::vector<Bunny*> visibleBunnies;
        std::vector<Bunny*> contactBunnies;

        b2Vec2 meanderDirection;
        b2Timer meanderTimer;

    public:
        Bunny();
        ~Bunny();

        static bool Initiate(float radius, unsigned int stacks, unsigned int slices, std::string bunnyModel, std::string zombieModel);
        static void Cleanup();

        bool Create(float x, float y, bool female, bool zombie, b2World* world);
        void Destroy(b2World* world);

        void Process();
        void Render();
        void RenderEyes();
        void RenderVision();

        void Pause();
        void Resume();

        void Impulse(float x, float y);
        void BecomeZombie();
        bool IsFemale();
        bool IsZombie();
        bool SeesBunny();
        bool SeesZombie();
        void BecomeAware(Bunny* otherBunny);
        void BecomeUnaware(Bunny* otherBunny);
        void BeginContact(Bunny* otherBunny);
        void EndContact(Bunny* otherBunny);
        b2Vec2 GetLocation();
        b2Vec2 GetVelocity();
        void Meander();
};

#endif
