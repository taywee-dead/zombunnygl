/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef SHAPE_HXX
#define SHAPE_HXX

#include <vector>
#include <fstream>
#include <string>
#include <sstream>

#include <Box2D/Box2D.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>

class Shape
{
    private:
        const static float PI = 3.1415926535;

        b2Body* body;
        b2Fixture* fixture;

        enum
        {
            SPHERE = 0,
            CUBE,
            VAOCOUNT
        };
        enum
        {
            Vertices = 0,
            Normals,
            Indices,
            NumVBOs
        };
        static GLuint buffers[VAOCOUNT][NumVBOs];
        static GLuint indexCount[VAOCOUNT];
        static GLuint VAO[VAOCOUNT];


        bool dynamic;
        bool rectangle;

        float halfWidth;
        float halfHeight;
        float radius;

    public:
        Shape();
        ~Shape();

        static bool Initiate(float radius, unsigned int stacks, unsigned int slices);
        static void Cleanup();

        bool CreateAsCircle(float x, float y, float radius, bool dynamic, b2World* world);
        bool CreateAsRectangle(float x, float y, float halfWidth, float halfHeight, bool dynamic, b2World* world);
        static Shape* CreateCircle(float x, float y, float radius, bool dynamic, b2World* world);
        static Shape* CreateRectangle(float x, float y, float halfWidth, float halfHeight, bool dynamic, b2World* world);
        void Destroy(b2World* world);

        void Process();
        void Render();

        void Pause();
        void Resume();

        void Impulse(float x, float y);
};

#endif
