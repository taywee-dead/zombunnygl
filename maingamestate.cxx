/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef MAINGAME_STATE_CXX
#define MAINGAME_STATE_CXX

#include "maingamestate.hxx"

MainGameState::MainGameState(GameCore* game) :
                black({0.0f, 0.0f, 0.0f, 1.0f}),
                white({1.0f, 1.0f, 1.0f, 1.0f})
{
    this->game = game;
}

void MainGameState::Create()
{
    GLfloat lightPosition[] = {75.0, 60.0, -10, 1.0};
    GLfloat lmodel_ambient[] = { 0.1, 0.1, 0.1, 1.0 };
    GLfloat white[] = { 1.0, 1.0, 1.0, 1.0 };

    glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, white);
    glLightfv(GL_LIGHT0, GL_SPECULAR, white);
    glMaterialf(GL_FRONT, GL_SHININESS, 70.0f);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_RESCALE_NORMAL);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glShadeModel(GL_SMOOTH);

    std::string bunnyModel;
    std::string zombieModel;
    game->GetModels(bunnyModel, zombieModel);
    Bunny::Initiate(1.0f, 10, 10, bunnyModel, zombieModel);
    Shape::Initiate(1.0f, 10, 10);
    showVision = false;
    velocityIterations = 6;
    positionIterations = 2;
    gravity = b2Vec2(0.0f, 0.0f);
    doSleep = true;
    world = new b2World(gravity);
    world->SetAllowSleeping(doSleep);
    contactListener = new ContactListener();
    world->SetContactListener(contactListener);

    defaultSpeed = 1.0f;
    scaleFactor = 5.0f;
    speedFactor = defaultSpeed;
    cameraCenterX = 75.0f;
    cameraCenterY = 60.0f;
    cameraZoom = 1.0f;

    shapeList = new std::vector<Shape*>();
    bunnyList = new std::vector<Bunny*>();

    shapeList->push_back(Shape::CreateRectangle(1.0f, 60.0f, 1.0f, 60.0f, false, world));
    shapeList->push_back(Shape::CreateRectangle(150.0f, 60.0f, 1.0f, 60.0f, false, world));
    shapeList->push_back(Shape::CreateRectangle(75.0f, 1.0f, 75.0f, 1.0f, false, world));
    shapeList->push_back(Shape::CreateRectangle(75.0f, 120.0f, 75.0f, 1.0f, false, world));

    for (unsigned short int i = 0; i < 200; i++)
    {
        bunnyList->push_back(new Bunny());
        bunnyList->back()->Create(rand() % 100 + 2, rand() % 100 + 2, (bool)(rand() % 2), !(bool)(rand() % 10), world);
    }

    SendResizeEvent();
}

void MainGameState::Destroy()
{
    while (!bunnyList->empty())
    {
        bunnyList->back()->Destroy(world);
        delete bunnyList->back();
        bunnyList->pop_back();
    }
    while (!shapeList->empty())
    {
        shapeList->back()->Destroy(world);
        delete shapeList->back();
        shapeList->pop_back();
    }
    Bunny::Cleanup();
    Shape::Cleanup();
    delete shapeList;
    delete bunnyList;
    delete world;
    delete contactListener;
}

void MainGameState::Pause()
{
    for (i = 0; i < bunnyList->size(); i++)
    {
        bunnyList->at(i)->Pause();
    }
    for (i = 0; i < shapeList->size(); i++)
    {
        shapeList->at(i)->Pause();
    }
    glPushAttrib(GL_COLOR_BUFFER_BIT | GL_LIGHTING_BIT | GL_ENABLE_BIT | GL_VIEWPORT_BIT);
    glPushClientAttrib(GL_CLIENT_VERTEX_ARRAY_BIT);
}

void MainGameState::Resume()
{
    for (i = 0; i < bunnyList->size(); i++)
    {
        bunnyList->at(i)->Resume();
    }
    for (i = 0; i < shapeList->size(); i++)
    {
        shapeList->at(i)->Resume();
    }

    glPopAttrib();
    glPopClientAttrib();
    SendResizeEvent();
}

void MainGameState::HandleEvents(GLFWEvent* event)
{
    switch (event->type)
    {
        case GLFWEvent::KEY:
            if (event->state == GLFW_PRESS)
            {
                switch (event->key)
                {
                    case GLFW_KEY_UP:
                        speedFactor *= (5.0f / 4.0f);
                        break;
                    case GLFW_KEY_DOWN:
                        speedFactor *= (4.0f / 5.0f);
                        break;
                    case GLFW_KEY_SPACE:
                        showVision = !showVision;
                        break;
                    case GLFW_KEY_ENTER:
                        speedFactor = defaultSpeed;
                        break;
                    case GLFW_KEY_ESC:
                        PushState(new PauseMenuState(game));
                        break;
                }
            }
            break;
        case GLFWEvent::BUTTON:
            if (event->state == GLFW_PRESS)
            {
                static GLint viewport[4];
                static GLdouble mvmatrix[16], projmatrix[16];
                static GLint realy;
                static GLdouble wx, wy, wz;
                glGetIntegerv(GL_VIEWPORT, viewport);
                glGetDoublev(GL_MODELVIEW_MATRIX, mvmatrix);
                glGetDoublev(GL_PROJECTION_MATRIX, projmatrix);
                realy = viewport[3] - event->y - 1;
                gluUnProject(event->x, (GLdouble) realy, 0.83f, mvmatrix, projmatrix, viewport, &wx, &wy, &wz);
                switch (event->button)
                {
                    case GLFW_MOUSE_BUTTON_LEFT:
                        bunnyList->push_back(new Bunny());
                        bunnyList->back()->Create(wx, wy, (bool)(rand() % 2), false, world);
                        break;
                    case GLFW_MOUSE_BUTTON_RIGHT:
                        bunnyList->push_back(new Bunny());
                        bunnyList->back()->Create(wx, wy, (bool)(rand() % 2), true, world);
                        break;
                    case GLFW_MOUSE_BUTTON_MIDDLE:
                        cameraCenterX = wx;
                        cameraCenterY = wy;
                        SendResizeEvent();
                        break;
                }
            }
            break;
        case GLFWEvent::MOUSE_POS:

            break;
        case GLFWEvent::MOUSE_WHEEL:
            if (event->direction > 0)
            {
                cameraZoom *= (10.0f / 9.0f);
            } else
            {
                cameraZoom *= (9.0f / 10.0f);
            }
            SendResizeEvent();
            break;
        case GLFWEvent::RESIZE:
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glViewport(0, 0, event->width, event->height);
            if (((GLfloat)event->width / (GLfloat)event->height) > (75.0f / 60.0f))
            {
                glFrustum(((GLfloat)event->width) / ((GLfloat)event->height) * (-60.0f / cameraZoom), ((GLfloat)event->width) / ((GLfloat)event->height) * (60.0f / cameraZoom), -60.0f / cameraZoom, 60.0f / cameraZoom, 80.0f, 200.0f);
            } else
            {
                glFrustum(-75.0f / cameraZoom, 75.0f / cameraZoom, ((GLfloat)event->height) / ((GLfloat)event->width) * (-75.0f / cameraZoom), ((GLfloat)event->height) / ((GLfloat)event->width) * (75.0f / cameraZoom), 80.0f, 200.0f);
            }
            gluLookAt(75.0f, 60.0f, -150.0, cameraCenterX, cameraCenterY, 0.0, 0.0, 1.0, 0.0);
            glMatrixMode(GL_MODELVIEW);
            glLoadIdentity();
            break;
        default:
            break;
    }
}

void MainGameState::Process(float frameTime)
{
    world->Step(frameTime * speedFactor, velocityIterations, positionIterations);
    for (i = 0; i < bunnyList->size(); i++)
    {
        bunnyList->at(i)->Process();
    }
    for (i = 0; i < shapeList->size(); i++)
    {
        shapeList->at(i)->Process();
    }
}

void MainGameState::Render()
{
    if (showVision)
    {
        glPushAttrib(GL_ENABLE_BIT | GL_DEPTH_BITS);
            glEnable(GL_BLEND);
            glDepthMask(GL_FALSE);
            glDisable(GL_LIGHTING);
            for (i = 0; i < bunnyList->size(); i++)
            {
                bunnyList->at(i)->RenderVision();
            }
        glPopAttrib();
    }

    for (i = 0; i < shapeList->size(); i++)
    {
        shapeList->at(i)->Render();
    }
    for (i = 0; i < bunnyList->size(); i++)
    {
        bunnyList->at(i)->Render();
    }
    glMaterialfv(GL_FRONT, GL_SPECULAR, white);
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
    for (i = 0; i < bunnyList->size(); i++)
    {
        bunnyList->at(i)->RenderEyes();
    }
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_FALSE);
    glMaterialfv(GL_FRONT, GL_SPECULAR, black);
    static std::stringstream output;
    output.str(std::string());
    output << "Bunnies:  " << bunnyList->size() << std::endl;
    output << "Speed Factor:  " << speedFactor;
    glColor3f(1.0f, 1.0f, 1.0f);
    DrawString(output.str(), 10, 64, 32);
}

#endif

