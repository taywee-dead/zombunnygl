/*
 *  Lead Coder:     Taylor C. Richberger
 */

#ifndef BUNNY_CXX
#define BUNNY_CXX

#include "bunny.hxx"

GLuint Bunny::indexCount[VAOCOUNT];
GLuint Bunny::buffers[VAOCOUNT][NumVBOs];
GLuint Bunny::VAO[VAOCOUNT];

Bunny::Bunny()
{
}

Bunny::~Bunny()
{
}

bool Bunny::Initiate(float radius, unsigned int stacks, unsigned int slices, std::string bunnyModel, std::string zombieModel)
{
    GLuint numVertices = 0;
    GLuint numFaces = 0;
    std::vector<GLfloat> vertices;
    std::vector<GLfloat> normals;
    std::vector<GLuint> indices;
    std::string inString;
    std::string dummy;
    std::string fileType;
    std::string fileVersion;
    GLfloat tempFloat;
    GLuint tempUInt;
    GLuint faceGon;
    std::ifstream input;
    glGenVertexArrays(VAOCOUNT, VAO);

    for (unsigned int stackNumber = 0; stackNumber <= stacks; ++stackNumber)
    {
        for (unsigned int sliceNumber = 0; sliceNumber < slices; ++sliceNumber)
        {
            float theta = stackNumber * PI / stacks;
            float phi = sliceNumber * 2 * PI / slices;
            float sinTheta = sin(theta);
            float sinPhi = sin(phi);
            float cosTheta = cos(theta);
            float cosPhi = cos(phi);
            vertices.push_back(radius * cosPhi * sinTheta);
            vertices.push_back(radius * sinPhi * sinTheta);
            vertices.push_back(radius * cosTheta);
        }
    }

    for (unsigned int stackNumber = 0; stackNumber < stacks; ++stackNumber)
    {
        for (unsigned int sliceNumber = 0; sliceNumber <= slices; ++sliceNumber)
        {
            indices.push_back((stackNumber * slices) + (sliceNumber % slices));
            indices.push_back(((stackNumber + 1) * slices) + (sliceNumber % slices));
        }
    }

    glBindVertexArray(VAO[SPHERE]);

        glGenBuffers(NumVBOs, buffers[SPHERE]);
        glBindBuffer(GL_ARRAY_BUFFER, buffers[SPHERE][Vertices]);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW);
        glVertexPointer(3, GL_FLOAT, 0, NULL);
        glEnableClientState(GL_VERTEX_ARRAY);

        glBindBuffer(GL_ARRAY_BUFFER, buffers[SPHERE][Normals]);
        glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW);
        glNormalPointer(GL_FLOAT, 0, NULL);
        glEnableClientState(GL_NORMAL_ARRAY);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[SPHERE][Indices]);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

    indexCount[SPHERE] = indices.size();

    glBindVertexArray(0);
    indices.clear();
    vertices.clear();
    normals.clear();

    input.open((std::string("models/") + bunnyModel + std::string("/head.ply")).c_str(), std::ifstream::in);
    if (input.is_open())
    {
        input >> inString;
        if (inString != "ply")
        {
            input.close();
            inString = "end_header";
        }
        input >> inString;
        while (inString != "end_header")
        {
            if (inString == "format")
            {
                input >> fileType;
                input >> fileVersion;
            } else if (inString == "element")
            {
                input >> inString;
                if (inString == "vertex")
                {
                    input >> inString;
                    std::stringstream(inString) >> numVertices;
                } else if (inString == "face")
                {
                    input >> inString;
                    std::stringstream(inString) >> numFaces;
                }
            } else if (inString == "property")
            {
                input >> inString;
                // ignore type for now
                input >> inString;
                if ((inString == "x") || (inString == "y") || (inString == "z"))
                {
                } else if ((inString == "nx") || (inString == "ny") || (inString == "nz"))
                {
                } else
                {
                    getline(input, dummy);
                }

            } else if (inString == "comment")
            {
                getline(input, dummy);
            } else
            {
                break;
            }
            input >> inString;
        }
        for (unsigned int i = 0; i < numVertices; i++)
        {
            for (unsigned int j = 0; j < 3; j++)
            {
                input >> inString;
                std::stringstream(inString) >> tempFloat;
                vertices.push_back(tempFloat);
            }
            for (unsigned int j = 0; j < 3; j++)
            {
                input >> inString;
                std::stringstream(inString) >> tempFloat;
                normals.push_back(tempFloat);
            }
        }
        // Only handles one type of face for now
        for (unsigned int i = 0; i < numFaces; i++)
        {
            input >> inString;
            std::stringstream(inString) >> faceGon;
            for (unsigned int j = 0; j < faceGon; j++)
            {
                input >> inString;
                std::stringstream(inString) >> tempUInt;
                indices.push_back(tempUInt);
            }
        }

        input.close();

        glBindVertexArray(VAO[BUNNYHEAD]);

            glGenBuffers(NumVBOs, buffers[BUNNYHEAD]);
            glBindBuffer(GL_ARRAY_BUFFER, buffers[BUNNYHEAD][Vertices]);
            glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW);
            glVertexPointer(3, GL_FLOAT, 0, NULL);
            glEnableClientState(GL_VERTEX_ARRAY);

            glBindBuffer(GL_ARRAY_BUFFER, buffers[BUNNYHEAD][Normals]);
            glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(GLfloat), &normals[0], GL_STATIC_DRAW);
            glNormalPointer(GL_FLOAT, 0, NULL);
            glEnableClientState(GL_NORMAL_ARRAY);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[BUNNYHEAD][Indices]);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);
            glVertexPointer(indices.size(), GL_UNSIGNED_INT, 0, NULL);

        indexCount[BUNNYHEAD] = indices.size();
        glBindVertexArray(0);
    } else
    {
        VAO[BUNNYHEAD] = VAO[SPHERE];
        buffers[BUNNYHEAD][Indices] = buffers[SPHERE][Indices];
        indexCount[BUNNYHEAD] = indexCount[SPHERE];
    }
    indices.clear();
    vertices.clear();
    normals.clear();

    input.open((std::string("models/") + bunnyModel + std::string("/eyes.ply")).c_str(), std::ifstream::in);
    if (input.is_open())
    {
        input >> inString;
        if (inString != "ply")
        {
            input.close();
            inString = "end_header";
        }
        input >> inString;
        while (inString != "end_header")
        {
            if (inString == "format")
            {
                input >> fileType;
                input >> fileVersion;
            } else if (inString == "element")
            {
                input >> inString;
                if (inString == "vertex")
                {
                    input >> inString;
                    std::stringstream(inString) >> numVertices;
                } else if (inString == "face")
                {
                    input >> inString;
                    std::stringstream(inString) >> numFaces;
                }
            } else if (inString == "property")
            {
                input >> inString;
                // ignore type for now
                input >> inString;
                if ((inString == "x") || (inString == "y") || (inString == "z"))
                {
                } else if ((inString == "nx") || (inString == "ny") || (inString == "nz"))
                {
                } else
                {
                    getline(input, dummy);
                }

            } else if (inString == "comment")
            {
                getline(input, dummy);
            } else
            {
                break;
            }
            input >> inString;
        }
        for (unsigned int i = 0; i < numVertices; i++)
        {
            for (unsigned int j = 0; j < 3; j++)
            {
                input >> inString;
                std::stringstream(inString) >> tempFloat;
                vertices.push_back(tempFloat);
            }
            for (unsigned int j = 0; j < 3; j++)
            {
                input >> inString;
                std::stringstream(inString) >> tempFloat;
                normals.push_back(tempFloat);
            }
        }
        for (unsigned int i = 0; i < numFaces; i++)
        {
            input >> inString;
            std::stringstream(inString) >> faceGon;
            for (unsigned int j = 0; j < faceGon; j++)
            {
                input >> inString;
                std::stringstream(inString) >> tempUInt;
                indices.push_back(tempUInt);
            }
        }

        input.close();
        glBindVertexArray(VAO[BUNNYEYES]);

            glGenBuffers(NumVBOs, buffers[BUNNYEYES]);
            glBindBuffer(GL_ARRAY_BUFFER, buffers[BUNNYEYES][Vertices]);
            glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW);
            glVertexPointer(3, GL_FLOAT, 0, NULL);
            glEnableClientState(GL_VERTEX_ARRAY);

            glBindBuffer(GL_ARRAY_BUFFER, buffers[BUNNYEYES][Normals]);
            glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(GLfloat), &normals[0], GL_STATIC_DRAW);
            glNormalPointer(GL_FLOAT, 0, NULL);
            glEnableClientState(GL_NORMAL_ARRAY);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[BUNNYEYES][Indices]);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

        indexCount[BUNNYEYES] = indices.size();

        glBindVertexArray(0);
    } else
    {
        VAO[BUNNYEYES] = 0;
        buffers[BUNNYEYES][Indices] = 0;
        indexCount[BUNNYEYES] = 0;
    }
    indices.clear();
    vertices.clear();
    normals.clear();

    input.open((std::string("models/") + zombieModel + std::string("/head.ply")).c_str(), std::ifstream::in);
    if (input.is_open())
    {
        input >> inString;
        if (inString != "ply")
        {
            input.close();
            inString = "end_header";
        }
        input >> inString;
        while (inString != "end_header")
        {
            if (inString == "format")
            {
                input >> fileType;
                input >> fileVersion;
            } else if (inString == "element")
            {
                input >> inString;
                if (inString == "vertex")
                {
                    input >> inString;
                    std::stringstream(inString) >> numVertices;
                } else if (inString == "face")
                {
                    input >> inString;
                    std::stringstream(inString) >> numFaces;
                }
            } else if (inString == "property")
            {
                input >> inString;
                // ignore type for now
                input >> inString;
                if ((inString == "x") || (inString == "y") || (inString == "z"))
                {
                } else if ((inString == "nx") || (inString == "ny") || (inString == "nz"))
                {
                } else
                {
                    getline(input, dummy);
                }

            } else if (inString == "comment")
            {
                getline(input, dummy);
            } else
            {
                break;
            }
            input >> inString;
        }
        for (unsigned int i = 0; i < numVertices; i++)
        {
            for (unsigned int j = 0; j < 3; j++)
            {
                input >> inString;
                std::stringstream(inString) >> tempFloat;
                vertices.push_back(tempFloat);
            }
            for (unsigned int j = 0; j < 3; j++)
            {
                input >> inString;
                std::stringstream(inString) >> tempFloat;
                normals.push_back(tempFloat);
            }
        }
        // Only handles one type of face for now
        for (unsigned int i = 0; i < numFaces; i++)
        {
            input >> inString;
            std::stringstream(inString) >> faceGon;
            for (unsigned int j = 0; j < faceGon; j++)
            {
                input >> inString;
                std::stringstream(inString) >> tempUInt;
                indices.push_back(tempUInt);
            }
        }

        input.close();

        glBindVertexArray(VAO[ZOMBIEHEAD]);

            glGenBuffers(NumVBOs, buffers[ZOMBIEHEAD]);
            glBindBuffer(GL_ARRAY_BUFFER, buffers[ZOMBIEHEAD][Vertices]);
            glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW);
            glVertexPointer(3, GL_FLOAT, 0, NULL);
            glEnableClientState(GL_VERTEX_ARRAY);

            glBindBuffer(GL_ARRAY_BUFFER, buffers[ZOMBIEHEAD][Normals]);
            glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(GLfloat), &normals[0], GL_STATIC_DRAW);
            glNormalPointer(GL_FLOAT, 0, NULL);
            glEnableClientState(GL_NORMAL_ARRAY);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[ZOMBIEHEAD][Indices]);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);
            glVertexPointer(indices.size(), GL_UNSIGNED_INT, 0, NULL);

        indexCount[ZOMBIEHEAD] = indices.size();
        glBindVertexArray(0);
    } else
    {
        VAO[ZOMBIEHEAD] = VAO[SPHERE];
        buffers[ZOMBIEHEAD][Indices] = buffers[SPHERE][Indices];
        indexCount[ZOMBIEHEAD] = indexCount[SPHERE];
    }
    indices.clear();
    vertices.clear();
    normals.clear();

    input.open((std::string("models/") + zombieModel + std::string("/eyes.ply")).c_str(), std::ifstream::in);
    if (input.is_open())
    {
        input >> inString;
        if (inString != "ply")
        {
            input.close();
            inString = "end_header";
        }
        input >> inString;
        while (inString != "end_header")
        {
            if (inString == "format")
            {
                input >> fileType;
                input >> fileVersion;
            } else if (inString == "element")
            {
                input >> inString;
                if (inString == "vertex")
                {
                    input >> inString;
                    std::stringstream(inString) >> numVertices;
                } else if (inString == "face")
                {
                    input >> inString;
                    std::stringstream(inString) >> numFaces;
                }
            } else if (inString == "property")
            {
                input >> inString;
                // ignore type for now
                input >> inString;
                if ((inString == "x") || (inString == "y") || (inString == "z"))
                {
                } else if ((inString == "nx") || (inString == "ny") || (inString == "nz"))
                {
                } else
                {
                    getline(input, dummy);
                }

            } else if (inString == "comment")
            {
                getline(input, dummy);
            } else
            {
                break;
            }
            input >> inString;
        }
        for (unsigned int i = 0; i < numVertices; i++)
        {
            for (unsigned int j = 0; j < 3; j++)
            {
                input >> inString;
                std::stringstream(inString) >> tempFloat;
                vertices.push_back(tempFloat);
            }
            for (unsigned int j = 0; j < 3; j++)
            {
                input >> inString;
                std::stringstream(inString) >> tempFloat;
                normals.push_back(tempFloat);
            }
        }
        for (unsigned int i = 0; i < numFaces; i++)
        {
            input >> inString;
            std::stringstream(inString) >> faceGon;
            for (unsigned int j = 0; j < faceGon; j++)
            {
                input >> inString;
                std::stringstream(inString) >> tempUInt;
                indices.push_back(tempUInt);
            }
        }

        input.close();
        glBindVertexArray(VAO[ZOMBIEEYES]);

            glGenBuffers(NumVBOs, buffers[ZOMBIEEYES]);
            glBindBuffer(GL_ARRAY_BUFFER, buffers[ZOMBIEEYES][Vertices]);
            glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLfloat), &vertices[0], GL_STATIC_DRAW);
            glVertexPointer(3, GL_FLOAT, 0, NULL);
            glEnableClientState(GL_VERTEX_ARRAY);

            glBindBuffer(GL_ARRAY_BUFFER, buffers[ZOMBIEEYES][Normals]);
            glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(GLfloat), &normals[0], GL_STATIC_DRAW);
            glNormalPointer(GL_FLOAT, 0, NULL);
            glEnableClientState(GL_NORMAL_ARRAY);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[ZOMBIEEYES][Indices]);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

        indexCount[ZOMBIEEYES] = indices.size();

        glBindVertexArray(0);
    } else
    {
        VAO[ZOMBIEEYES] = 0;
        buffers[ZOMBIEEYES][Indices] = 0;
        indexCount[ZOMBIEEYES] = 0;
    }
    indices.clear();
    vertices.clear();
    normals.clear();

    return (true);
}

void Bunny::Cleanup()
{
    for (unsigned int i = 0; i < VAOCOUNT; i++)
    {
        glBindVertexArray(i);
        glDeleteBuffers(NumVBOs, buffers[i]);
    }
    glDeleteVertexArrays(VAOCOUNT, VAO);
}

bool Bunny::Create(float x, float y, bool female, bool zombie, b2World* world)
{
    angle = 0;
    seesZombie = false;
    seesBunny = false;

    this->female = female;
    this->zombie = zombie;
    do
    {
        meanderDirection.x = (rand() % 51) - 25;
        meanderDirection.y = (rand() % 51) - 25;
    } while ((meanderDirection.x == 0) && (meanderDirection.y == 0));
    b2BodyDef* bodyDef = new b2BodyDef();
    bodyDef->position.Set(x, y);
    b2CircleShape* bunnyShape = new b2CircleShape();
    bunnyShape->m_radius = 1.0f;

    b2CircleShape* visionShape = new b2CircleShape();
    visionShape->m_radius = 10.0f;

    b2FixtureDef* bunnyFixtureDef = new b2FixtureDef();
    bunnyFixtureDef->shape = bunnyShape;

    bunnyFixtureDef->density = 1.0f;
    bunnyFixtureDef->friction = 0.0f;
    bunnyFixtureDef->restitution = 0.025f;

    b2FixtureDef* visionFixtureDef = new b2FixtureDef();
    visionFixtureDef->shape = visionShape;
    visionFixtureDef->isSensor = true;

    visionFixtureDef->density = 0.0f;
    visionFixtureDef->friction = 0.0f;
    visionFixtureDef->restitution = 0.0f;

    bodyDef->type = b2_dynamicBody;
    bodyDef->linearDamping = 1.0f;
    body = world->CreateBody(bodyDef);
    body->SetFixedRotation(true);
    body->SetUserData((void*)this);
    bunnyFixture = body->CreateFixture(bunnyFixtureDef);
    visionFixture = body->CreateFixture(visionFixtureDef);
    bunnyFixture->SetUserData((void*)1);
    visionFixture->SetUserData((void*)2);

    delete bodyDef;
    delete bunnyFixtureDef;
    delete visionFixtureDef;
    delete bunnyShape;
    delete visionShape;
    return (true);
}

void Bunny::Destroy(b2World* world)
{
    world->DestroyBody(body);
}

void Bunny::Process()
{
    static b2Vec2 force(0.0f, 0.0f);
    force.x = 0.0f;
    force.y = 0.0f;

    seesBunny = false;
    seesZombie = false;

    if (zombie)
    {
        // Analyze all bunnies seen
        for (i = 0; i < visibleBunnies.size(); i++)
        {
            // If current bunny is not a zombie
            if (!visibleBunnies[i]->IsZombie())
            {
                // Set value for first seen bunny
                if (!seesBunny)
                {
                    force = body->GetLocalPoint(visibleBunnies[i]->GetLocation());
                    seesBunny = true;
                }

                // Find closest bunny this zombie sees
                if (body->GetLocalPoint(visibleBunnies[i]->GetLocation()).Length() < force.Length())
                {
                    force = body->GetLocalPoint(visibleBunnies[i]->GetLocation());
                }
            }
        }

        if (!seesBunny)
        {
            for (i = 0; i < visibleBunnies.size(); i++)
            {
                // Chases same average direction as all visible zombies who see bunnies
                if (visibleBunnies[i]->SeesBunny())
                {
                    force += visibleBunnies[i]->GetVelocity();
                }
                seesZombie = true;
            }
        }


        // Prevent any possible divide by zero errors
        if ((force.x != 0.0f) || (force.y != 0.0f))
        {
            // Chase closest bunny
            body->ApplyForceToCenter((8.0f / force.Length()) * force);
        } else
        // Wander randomly if this zombie isn't moving
        {
            Meander();
            body->ApplyForceToCenter((4.0f / meanderDirection.Length()) * meanderDirection);
        }
    } else
    {
        // Analyze all bunnies seen
        for (i = 0; i < visibleBunnies.size(); i++)
        {
            // If current bunny is a zombie
            if (visibleBunnies[i]->IsZombie())
            {
                // Find center of masses of visible zombies
                force += body->GetLocalPoint(visibleBunnies[i]->GetLocation());
                seesZombie = true;
            }
        }

        if (!seesZombie)
        {
            for (i = 0; i < visibleBunnies.size(); i++)
            {
                // Find center of masses of bunnies who see zombies
                if (visibleBunnies[i]->SeesZombie())
                {
                    force += body->GetLocalPoint(visibleBunnies[i]->GetLocation());
                    // seesBunny only refers to bunnies who see zombies at this point
                    seesBunny = true;
                }
            }
        }

        // If this bunny doesn't see a zombie or any bunnies who see zombies
        if (!seesZombie && !seesBunny)
        {
            for (i = 0; i < visibleBunnies.size(); i++)
            {
                // If current bunny is not the same gender as this bunny
                if (female != visibleBunnies[i]->IsFemale())
                {
                    // Set value for first seen bunny
                    if (!seesBunny)
                    {
                        force = -body->GetLocalPoint(visibleBunnies[i]->GetLocation());
                        seesBunny = true;
                    }

                    // Find closest opposite gender bunny this bunny sees
                    if (body->GetLocalPoint(visibleBunnies[i]->GetLocation()).Length() < force.Length())
                    {
                        force = -body->GetLocalPoint(visibleBunnies[i]->GetLocation());
                    }
                }
            }
        }

        // If force is not zero
        if ((force.x != 0.0f) || (force.y != 0.0f))
        {
            // Run directly away from center of mass of zombies, defined by force
            // Will also run from bunnies who can see zombies, if no zombies ar visible
            body->ApplyForceToCenter(-(10.0f / force.Length()) * force);
        }
    }
    if (body->GetLinearVelocity().Length() > 1.0f)
    {
        angle = atan2f(body->GetLinearVelocity().y, body->GetLinearVelocity().x) * 180.0f / PI;
    }
}

void Bunny::Render()
{
    static GLfloat rotMatrix[] = {1, 0, 0, 0,
                                   0, 0, -1, 0,
                                   0, 1, 0, 0,
                                   0, 0, 0, 1};
    static GLuint current;
    if (zombie)
    {
        current = ZOMBIEHEAD;
        if (female)
        {
            // R + B = Magenta
            glColor3f(1.0f, 0.0f, 1.0f);
        } else
        {
            // G
            glColor3f(0.0f, 1.0f, 0.0f);
        }
    } else
    {
        current = BUNNYHEAD;
        if (female)
        {
            // R + G + B = White
            glColor3f(1.0f, 1.0f, 1.0f);
        } else
        {
            // Light Blue
            glColor3f(0.5f, 0.5f, 1.0f);
        }
    }
    glBindVertexArray(VAO[current]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[current][Indices]);
    glPushMatrix();
        glTranslatef(body->GetWorldCenter().x, body->GetWorldCenter().y, 0.0f);
        glRotatef(angle - 90.0f, 0.0f, 0.0f, 1.0f);
        glMultMatrixf(rotMatrix);
        glDrawElements(GL_TRIANGLES, indexCount[current], GL_UNSIGNED_INT, 0);
    glPopMatrix();
    glBindVertexArray(0);
}

void Bunny::RenderEyes()
{
    static GLfloat rotMatrix[] = {1, 0, 0, 0,
                                   0, 0, -1, 0,
                                   0, 1, 0, 0,
                                   0, 0, 0, 1};
    static GLuint current;
    if (zombie)
    {
        current = ZOMBIEEYES;
        glColor3f(1.0f, 0.0f, 0.0f);
    } else
    {
        current = BUNNYEYES;
        glColor3f(0.0f, 0.0f, 0.0f);
    }
    glBindVertexArray(VAO[current]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[current][Indices]);
    glPushMatrix();
        glTranslatef(body->GetWorldCenter().x, body->GetWorldCenter().y, 0.0f);
        glRotatef(angle - 90.0f, 0.0f, 0.0f, 1.0f);
        glMultMatrixf(rotMatrix);
        glDrawElements(GL_TRIANGLES, indexCount[current], GL_UNSIGNED_INT, 0);
    glPopMatrix();
    glBindVertexArray(0);
}

void Bunny::RenderVision()
{
    glBindVertexArray(VAO[SPHERE]);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[SPHERE][Indices]);
    glColor4f(1.0f, 1.0f, 0.0f, 0.20f);
    glPushMatrix();
        glTranslatef(body->GetWorldCenter().x, body->GetWorldCenter().y, 0.0f);
        glScalef(10.0f, 10.0f, 10.0f);
        glDrawElements(GL_TRIANGLE_STRIP, indexCount[SPHERE], GL_UNSIGNED_INT, NULL);
    glPopMatrix();
    glBindVertexArray(0);
}

void Bunny::Pause()
{
}

void Bunny::Resume()
{

}

void Bunny::Impulse(float x, float y)
{
    body->SetLinearVelocity(b2Vec2(x, y) + body->GetLinearVelocity());
}

void Bunny::BecomeZombie()
{
    // Makes bunny zombie
    zombie = true;

    if (female)
    {
    }

    // Make all contacting non-zombie bunnies zombies
    for (i = 0; i < contactBunnies.size(); i++)
    {
        if (!contactBunnies[i]->IsZombie())
        {
            contactBunnies[i]->BecomeZombie();
        }
    }
}

bool Bunny::IsFemale()
{
    return (female);
}

bool Bunny::IsZombie()
{
    return (zombie);
}

bool Bunny::SeesBunny()
{
    return (seesBunny);
}

bool Bunny::SeesZombie()
{
    return (seesZombie);
}

void Bunny::BecomeAware(Bunny* otherBunny)
{
    visibleBunnies.push_back(otherBunny);
}

void Bunny::BecomeUnaware(Bunny* otherBunny)
{
    for (i = 0; i < visibleBunnies.size(); i++)
    {
        if (otherBunny == visibleBunnies[i])
        {
            visibleBunnies.erase(visibleBunnies.begin() + i);
            break;
        }
    }
}

void Bunny::BeginContact(Bunny* otherBunny)
{
    contactBunnies.push_back(otherBunny);
    if (zombie)
    {
        otherBunny->BecomeZombie();
    }
}

void Bunny::EndContact(Bunny* otherBunny)
{
    for (i = 0; i < contactBunnies.size(); i++)
    {
        if (otherBunny == contactBunnies[i])
        {
            contactBunnies.erase(contactBunnies.begin() + i);
            break;
        }
    }
}

b2Vec2 Bunny::GetLocation()
{
    return(body->GetWorldCenter());
}

b2Vec2 Bunny::GetVelocity()
{
    return(body->GetLinearVelocity());
}

void Bunny::Meander()
{
    // Only changes directions every 3 seconds
    if (meanderTimer.GetMilliseconds() > 3000)
    {
        meanderTimer.Reset();
        // Sets each of meanderDirection's components to a random number ranging from -25 to 25,
        //+does not let both be zero
        do
        {
            meanderDirection.x = (rand() % 51) - 25;
            meanderDirection.y = (rand() % 51) - 25;
        } while ((meanderDirection.x == 0) && (meanderDirection.y == 0));
    }
}

#endif
